ABOUT MODULE
------------

The module allows adding extra displaying settings for fields.

E.g. we have some the same bean blocks displayed as the header block on several pages. An editor can manage the displaying fields in these blocks.

Available features for the site editor:
<ul>
  <li>Hide the field from displaying</li>
  <li>Add an extra class that was pre styled, so that field will highlight, or some special icon will appear before the field</li>
  <li>Manage field displaying for all user roles</li>
  <li>Manage image field, by adding different available image caches</li>
</ul>

The module supports multiple fields.
Functionality can be extended by adding some custom availabilities depends on requirements by editing field_extra_settings.admin.inc.

// TODO
- Add support for the paragraphs.
- Move field_extra_settings.admin.inc to admin pages.